% !TEX TS-program = XeLaTeX
% !TEX encoding = UTF-8 Unicode

\chapter{流固共轭传热算法}
\label{chap02}

\section{低压缸共轭传热解法概述}

对于像汽轮机中蒸汽与汽缸的对流换热问题，汽缸壁面热边界条件是流体和壁面相互作用的结果，无法作为计算条件给定。不同域交界面上的温度、热流、换热系数等参数都是计算结果，而不应事先规定。这种热边界条件是由热交换过程动态决定而不是事先规定的问题，称为共轭传热问题（Conjugate heat transfer）。

实际的共轭传热问题，因为其复杂性，很难获得解析解，只能采用数值解法。数值解法分为两种：分区求解、边界耦合和整场求解两种方法\cite{RN35}。

分区求解、边界耦合和步骤是：
\begin{enumerate}
	\item 对各个域分别建立控制方程；
	\item 规定各域的边界条件，其中耦合交界面的条件可从下列三种中选取两个：

		\subitem $ \textcircled 1 $耦合边界温度连续：
		
		\begin{equation}
		{{T}_{W}}\left| _{1}\right. ={{T}_{W}}\left| _{2} \right.
		\end{equation}
		
		\subitem $ \textcircled 2 $耦合边界热流密度连续：
		
		\begin{equation}
		{{q}_{W}}\left| _{1}={{q}_{W}}\left| _{2} \right. \right.
		\end{equation}
		
		\subitem $ \textcircled 3 $耦合边界内第三类边界条件：
		
		\begin{equation}
		-\lambda {{\left( \frac{\partial T}{\partial n} \right)}_{W}}\left| _{1}=h\left( {{T}_{W}}-{{T}_{f}} \right) \right.\left| _{2} \right.
		\end{equation}
		
式中：$ T_W $为耦合边界温度；$ q_W $为热流密度；$ \lambda $为固体域热导率；$ h $为对流换热系数；$ T_f $为流体主流温度；$ n $为壁面外法线。

	\item 预估边界上的边界条件，对其中一个域进行求解，将求解结果作为边界条件求解另一个域，迭代直至收敛。
\end{enumerate}

整场求解是目前共轭传热问题的主流方法，该方法将问题中的不同区域的传热过程统一为一个换热过程，各个域使用通用控制方程描述，通过定义不同的广义扩散项和广义源项进行区分\cite{RN36}。这样耦合面就不再是计算边界，而是计算域内部，并且可以满足连续性条件。相较于分区求解、边界耦合，省去了反复迭代的过程，显著地减少了计算时间。

共轭传热问题的解决步骤如图\ref{fig:共轭传热问题整体求解流程}，下面从控制方程部分开始。

\begin{figure}[htb]
	\centering
	\includegraphics[height=11.6cm]{chap01/1.png}
	\bicaption[fig:共轭传热问题整体求解流程]{图}{ \textbf{共轭传热问题整体求解流程}}{Fig.}{Conjugate heat transfer problem solving process}
\end{figure}

\section{控制方程组}

对于汽轮机内部蒸汽流动，可使用流体基本状态方程描述。在直角坐标系中，设流体速度矢量为$ \textbf{\textit{U}} $，在三个坐标方向的分量为$ u $，$ v $，$ w $，密度为$ \rho $，压力为$ p $。并且，$ u $、$ v $、$ w $、$ p $、$ ρ $均为时间及空间的函数。下面直接给出描述流动的质量守恒方程、动量守恒方程和能量守恒方程。

质量守恒方程：

\begin{equation}
\frac{\partial \rho }{\partial t}+\frac{\partial \left( \rho u \right)}{\partial x}+\frac{\partial \left( \rho v \right)}{\partial y}+\frac{\partial \left( \rho w \right)}{\partial z}=0
\end{equation}

动量守恒方程：

\begin{equation}
\label{eq:2.5}
\begin{align}
& \frac{\partial \left( \rho u \right)}{\partial t}+\frac{\partial \left( uu \right)}{\partial x}+\frac{\partial \left( vu \right)}{\partial y}+\frac{\partial \left( wu \right)}{\partial z}=-\frac{\partial p}{\partial x}+\frac{\partial }{\partial x}\left( \lambda \text{div}\textbf{\textit{U}}+2\eta \frac{\partial u}{\partial x} \right)+ \\ 
& \frac{\partial }{\partial y}\left[ \eta \left( \frac{\partial v}{\partial x}+\frac{\partial u}{\partial y} \right) \right]+\frac{\partial }{\partial z}\left[ \eta \left( \frac{\partial u}{\partial z}+\frac{\partial w}{\partial x} \right) \right]+\rho {{F}_{x}} \\ 
\end{align}
\end{equation}

\begin{equation}
\begin{align}
& \frac{\partial \left( \rho v \right)}{\partial t}+\frac{\partial \left( uv \right)}{\partial x}+\frac{\partial \left( vv \right)}{\partial y}+\frac{\partial \left( wv \right)}{\partial z}=-\frac{\partial p}{\partial y}+\frac{\partial }{\partial x}\left[ \eta \left( \frac{\partial u}{\partial y}+\frac{\partial v}{\partial x} \right) \right]+ \\ 
& \frac{\partial }{\partial y}\left( \lambda \text{div}\textbf{\textit{U}}+2\eta \frac{\partial v}{\partial y} \right)+\frac{\partial }{\partial z}\left[ \eta \left( \frac{\partial v}{\partial z}+\frac{\partial w}{\partial y} \right) \right]+\rho {{F}_{y}} \\ 
\end{align}
\end{equation}

\begin{equation}
\label{eq:2.7}
\begin{align}
& \frac{\partial \left( \rho w \right)}{\partial t}+\frac{\partial \left( uw \right)}{\partial x}+\frac{\partial \left( vw \right)}{\partial y}+\frac{\partial \left( ww \right)}{\partial z}=-\frac{\partial p}{\partial z}+\frac{\partial }{\partial x}\left[ \eta \left( \frac{\partial u}{\partial z}+\frac{\partial w}{\partial x} \right) \right]+ \\ 
& \frac{\partial }{\partial y}\left[ \eta \left( \frac{\partial v}{\partial z}+\frac{\partial w}{\partial y} \right) \right]+\frac{\partial }{\partial z}\left( \lambda \text{div}\textbf{\textit{U}}+2\eta \frac{\partial w}{\partial z} \right)+\rho {{F}_{z}} \\ 
\end{align}
\end{equation}

式中：$ \eta $为动力粘度，$ \lambda $为流体的第二分子黏度。

div为求散度:

\begin{equation}
\text{div}\textbf{\textit{U}}\text{=}\frac{\partial u}{\partial x}+\frac{\partial v}{\partial y}+\frac{\partial w}{\partial z}
\end{equation}

能量守恒方程：

\begin{equation}
\frac{\partial \left( \rho h \right)}{\partial t}+\frac{\partial \left( \rho uh \right)}{\partial x}+\frac{\partial \left( \rho vh \right)}{\partial y}+\frac{\partial \left( \rho wh \right)}{\partial z}=-p\text{div}\mathbf{U}+\text{div}\left( \lambda \text{grad}T \right)+\Phi +{{S}_{h}}
\end{equation}

其中$ \CYRF $为黏性作用导致动能转化为热能的部分，计算公式如下：

\begin{equation}
\begin{align}
& \Phi =\eta \left\{ 2\left[ {{\left( \frac{\partial u}{\partial x} \right)}^{2}}+{{\left( \frac{\partial v}{\partial y} \right)}^{2}}+{{\left( \frac{\partial w}{\partial z} \right)}^{2}} \right]+{{\left( \frac{\partial u}{\partial y}+\frac{\partial v}{\partial x} \right)}^{2}}+{{\left( \frac{\partial u}{\partial z}+\frac{\partial w}{\partial x} \right)}^{2}}+{{\left( \frac{\partial v}{\partial z}+\frac{\partial w}{\partial y} \right)}^{2}} \right\} \\ 
& \text{      }+\lambda \text{div}\mathsf{U} \\ 
\end{align}
\end{equation}

式(\ref{eq:2.5})$ \sim $(\ref{eq:2.7})是三维非稳态的纳维-斯托克斯方程，对层流和湍流都能适用。但是如果直接求解湍流三维非稳态控制方程，需要采用直接模拟法（Direct Numencal Simulation），此方法对计算设备要求极高，目前无法在工程中应用\cite{RN37}。从需求角度看，需要知道的是平均流场的变化。雷诺提出将所有变量做时间平均的方程，将方程分为平均部分和脉动部分，避免了直接模拟计算困难的问题\cite{RN38}。雷诺平均是现阶段应用最多的湍流模拟方法。为了使平均状态方程封闭，还需要补充湍流模型。

本文使用$ k-\varepsilon $湍流模型。其输运方程为：

\begin{equation}
\frac{\partial \left( \rho k \right)}{\partial t}+\frac{\partial \left( \rho k{{u}_{i}} \right)}{\partial {{x}_{i}}}=\frac{\partial }{\partial {{x}_{j}}}\left[ \left( \mu +\frac{{{\mu }_{t}}}{{{\sigma }_{k}}} \right)\frac{\partial k}{\partial {{x}_{j}}} \right]+{{G}_{k}}+{{G}_{b}}-\rho \varepsilon -{{Y}_{\text{M}}}+{{S}_{k}}
\end{equation}

\begin{equation}
\frac{\partial \left( \rho \varepsilon  \right)}{\partial t}+\frac{\partial \left( \rho \varepsilon {{u}_{i}} \right)}{\partial {{x}_{i}}}=\frac{\partial }{\partial {{x}_{j}}}\left[ \left( \mu +\frac{{{\mu }_{t}}}{{{\sigma }_{\varepsilon }}} \right)\frac{\partial \varepsilon }{\partial {{x}_{j}}} \right]+{{C}_{1\varepsilon }}\frac{\varepsilon }{k}\left( {{G}_{k}}+{{C}_{3\varepsilon }}{{G}_{b}} \right)-{{C}_{2\varepsilon }}\rho \frac{{{\varepsilon }^{2}}}{k}+{{S}_{\varepsilon }}
\end{equation}

湍流应力按下式计算：

\begin{equation}
{{\left( {{\tau }_{i,j}} \right)}_{t}}=\lambda \rho k{{\delta }_{i,j}}+{{\eta }_{t}}\left( \frac{\partial {{u}_{i}}}{\partial {{x}_{j}}}+\frac{\partial {{u}_{i}}}{\partial {{x}_{j}}} \right)
\end{equation}

\begin{equation}
{{\eta }_{t}}={{c}_{p}}\rho {{k}^{2}}/\varepsilon 
\end{equation}

式中，$ G_k $为速度梯度引起的湍动能$ k $的产生项；$ G_b $为浮力引起的湍动能$ k $的产生项；$ Y_M $为湍动脉动扩张的产生项；$ C_{1\varepsilon} $ 、$ C_{2\varepsilon} $ 、$ C_{3\varepsilon} $为经验系数；$ \delta_k $ 、$ \delta_\epsilon $为湍动能$ k $和耗散率$ \epsilon $所对应的普朗特数；$ S_k $ 、$ S_\varepsilon $为用户定义的源项；$ \mu_t $为湍动粘度，下标$ t $表示时均项。

$ k-\varepsilon $中方程常数是从Brian Launder工作中得出的\cite{RN39}，取值见表 2.1。

\begin{table}[htbp]
	\bicaption[tab:The values of the constants in the $ k-\varepsilon $ model]{表}{$ k-\varepsilon $方程常数}{Tab.}{The values of the constants in the $ k-\varepsilon $ model}
	\centering
	\vspace{0.2cm}
	\zhongwu
	\begin{tabular}{p{1.5cm}<{\centering}p{1.5cm}<{\centering}p{1.5cm}<{\centering}p{1.5cm}<{\centering}p{1.5cm}<{\centering}p{2cm}<{\centering}}
		\toprule
		$ c_\mu $ & $ c_1 $ & $ c_2 $ & $ \sigma_k $ & $ \sigma_\varepsilon $ & $ \sigma_T $  \\
		\midrule
		$ 0.09 $ & $ 1.44 $ & $ 1.92 $ & $ 1 $ & $ 1.3 $ & $ 0.9\sim1.0 $ \\
		\bottomrule
	\end{tabular}
\end{table}

低压缸中蒸汽不考虑浮力，也没有其它需要自定义的源项。

对于汽缸和持环，固体域的导热方程与流体的N-S方程类似，只是固体域不需处理流动部分。固体域的导热方程为后者的特定情况。因此，只要对流体方程中的粘度和热导率进行重新定义，就可使其适用于固体域的导热过程。

对公式中的粘度分段定义，在流体域内，粘度为流体本身的粘度。在固体区域，将粘度设定为无穷大，再在边界处给定的速度为0的初始场，就满足了固体域内无运动的特性。在温度场计算时，固体域和流体域的热导率分别计算，然后在交界面处耦合。

以上所用的控制方程组，都可以用统一的通用控制方程表示，形式如下：

\begin{equation}
\label{eq:2.15}
\frac{\partial \left( \rho \phi  \right)}{\partial t}+\frac{\partial \left( \rho u\phi  \right)}{\partial x}+\frac{\partial \left( \rho v\phi  \right)}{\partial y}+\frac{\partial \left( \rho w\phi  \right)}{\partial z}=\frac{\partial }{\partial x}\left( \varGamma \frac{\partial \phi }{\partial x} \right)+\frac{\partial }{\partial y}\left( \varGamma \frac{\partial \phi }{\partial y} \right)+\frac{\partial }{\partial z}\left( \varGamma \frac{\partial \phi }{\partial z} \right)+{{S}_{\phi }}
\end{equation}

式中$ \phi $代指待求解变量。$ \phi $、$ \varGamma $ 、 $ S_\phi $所代表的含义见表\ref{tab:2.2}。表中：

\begin{equation}
{{\eta }_{eff}}=\eta +{{\eta }_{t}}
\end{equation}

\begin{table}[htbp]
	\bicaption[tab:2.2]{表}{通用方程中 $ \phi $、$ \varGamma $、$ S_\varphi $含义}{Tab.}{Meaning of $ \phi $、$ \varGamma $、$ S_\varphi $ in general equation}
	\centering
	\zhongwu
	\vspace{0.2cm}
	\begin{tabular}{cccc}
		\toprule
		方程 & $ \phi $ & $ \varGamma $ & $ S_\phi $  \\
		\midrule
		质量守恒方程 & 1 & 0 & 0  \\
		u-动量方程 & $ u $ & $ \eta_{eff} $ & $ \displaystyle -\frac{\partial p}{\partial x}+\frac{\partial}{\partial x} \left(\eta_{eff} \frac{\partial u}{\partial x}\right)+\frac{\partial}{\partial x} \left(\eta_{eff} \frac{\partial v}{\partial x}\right)+\frac{\partial}{\partial x} \left(\eta_{eff} \frac{\partial w}{\partial x}\right) $    \\
		v-动量方程 & $ v $ & $ \eta_{eff} $ & $ \displaystyle -\frac{\partial p}{\partial y}+\frac{\partial}{\partial y} \left(\eta_{eff} \frac{\partial u}{\partial y}\right)+\frac{\partial}{\partial y} \left(\eta_{eff} \frac{\partial v}{\partial y}\right)+\frac{\partial}{\partial y} \left(\eta_{eff} \frac{\partial w}{\partial y}\right) $  \\
		w-动量方程 & $ w $ & $ \eta_{eff} $ & $ \displaystyle -\frac{\partial p}{\partial z}+\frac{\partial}{\partial z} \left(\eta_{eff} \frac{\partial u}{\partial z}\right)+\frac{\partial}{\partial z} \left(\eta_{eff} \frac{\partial v}{\partial z}\right)+\frac{\partial}{\partial z} \left(\eta_{eff} \frac{\partial w}{\partial z}\right) $  \\
		能量守恒方程 & $ T $ & $ \displaystyle \frac{\eta}{Pr}+\frac{\eta_t}{\delta_T} $ & 0  \\
		湍动能方程 & $ k $ & $ \displaystyle \eta+\frac{\eta_t}{\delta_k} $ & $ G_k - \rho \varepsilon $  \\
		湍流耗散率 & $ \varepsilon $ & $ \displaystyle \eta+\frac{\eta_t}{\delta_\varepsilon} $ & 1  \\
		\bottomrule
	\end{tabular}
\end{table}

\section{离散化和数值求解过程}

按照控制方程的离散方法不同，CFD数值解法主要分为三种：有限差分法、有限元法和有限体积法\cite{RN40}。有限体积法的基本思路是：将计算域依照空间离散划分控制体积，每个节点对应一个控制体积，通过将控制方程对控制体积分求得离散方程\cite{RN41}。有限体积法的基本思路有直接的物理解释，其物理意义就是在每个控制体积中，参数都遵循守恒原理。

有限体积法是一种分块求解计算的方法，因此计算区域的离散和控制方程的离散对求解非常重要。计算区域的离散就是划分网格的过程，使用网格节点代替计算域，但控制单元不是网格网格，而是节点周围的控制体积，每个节点控制一个控制体积。在离散过程中，将整个控制体的参数储存在该节点处\cite{RN42}。图 \ref{fig:二维计算区域离散}为二维网格的离散示意图。

计算域离散的网格分两类：结构化网格和非结构化网格。结构化网格的优点在于计算中，计算精度高、速度快，其缺点在于划分网格的时候，结构适应性差；非结构化网格则正好相反。例如，对于像图 3.7中（b）（e）这样结构复杂还需要添加边界层的模型，划分结构化网格将花费大量的时间且质量难以保证，而借用商用软件就可以快速地划分出质量优秀的非结构化网格。

\begin{figure}[htb]
	\centering
	\includegraphics[height=6.4cm]{chap02/1.png}
	\bicaption[fig:二维计算区域离散]{图}{ \textbf{二维计算区域离散}}{Fig.}{Discretization of 2D computational region}
\end{figure}

前文给出通用方程组\ref{eq:2.15}可用式\ref{eq:2.17}的简化形式表示：

\begin{equation}
\frac{\partial \left( \rho u\phi  \right)}{\partial t}+\text{div}\left( \rho u\phi  \right)=\text{div}\left( \Gamma \text{grad}\phi  \right)+{{S}_{\phi }}
\label{eq:2.17}
\end{equation}

取控制体积，如图 2.3。将通用方程的不同项分别离散、差分并整理后，得到三维离散方程如下\cite{RN43}：

\begin{figure}[htb]
	\centering
	\includegraphics[height=7.7cm]{chap02/2.png}
	\bicaption[fig:三维控制体积]{图}{ \textbf{三维控制体积}}{Fig.}{Three-dimensional control volume}
\end{figure}

\begin{equation}
{{a}_{P}}{{\phi }_{P}}={{a}_{W}}{{\phi }_{W}}+{{a}_{E}}{{\phi }_{E}}+{{a}_{N}}{{\phi }_{N}}+{{a}_{S}}{{\phi }_{S}}+{{a}_{T}}{{\phi }_{T}}+{{a}_{B}}{{\phi }_{B}}+b
\end{equation}

其中：

\begin{equation}
\left\{ \begin{align}
& {{a}_{P}}={{a}_{E}}+{{a}_{W}}+{{a}_{N}}+{{a}_{S}}+{{a}_{T}}+{{a}_{B}}-{{S}_{P}}\text{ }\!\!\delta\!\!\text{ }x\text{ }\!\!\delta\!\!\text{ }y\text{ }\!\!\delta\!\!\text{ }z \\ 
& {{a}_{W}}={{D}_{w}}\left\{ \left[ \left| 0,{{\left( 1-0.1\left| P{{e}_{w}} \right| \right)}^{5}} \right| \right]+\left( \left| P{{e}_{w}},0 \right| \right) \right\} \\ 
& {{a}_{E}}={{D}_{e}}\left\{ \left[ \left| 0,{{\left( 1-0.1\left| P{{e}_{e}} \right| \right)}^{5}} \right| \right]+\left( \left| -P{{e}_{e}},0 \right| \right) \right\} \\ 
& {{a}_{N}}={{D}_{n}}\left\{ \left[ \left| 0,{{\left( 1-0.1\left| P{{e}_{n}} \right| \right)}^{5}} \right| \right]+\left( \left| -P{{e}_{n}},0 \right| \right) \right\} \\ 
& {{a}_{S}}={{D}_{s}}\left\{ \left[ \left| 0,{{\left( 1-0.1\left| P{{e}_{s}} \right| \right)}^{5}} \right| \right]+\left( \left| P{{e}_{s}},0 \right| \right) \right\} \\ 
& {{a}_{T}}={{D}_{t}}\left\{ \left[ \left| 0,{{\left( 1-0.1\left| P{{e}_{t}} \right| \right)}^{5}} \right| \right]+\left( \left| -P{{e}_{t}},0 \right| \right) \right\} \\ 
& {{a}_{B}}={{D}_{b}}\left\{ \left[ \left| 0,{{\left( 1-0.1\left| P{{e}_{b}} \right| \right)}^{5}} \right| \right]+\left( \left| P{{e}_{b}},0 \right| \right) \right\} \\ 
& b={{S}_{c}}\text{ }\!\!\delta\!\!\text{ }x\text{ }\!\!\delta\!\!\text{ }y\text{ }\!\!\delta\!\!\text{ }z \\ 
\end{align} \right.
\end{equation}

$ Pe $为Peclet数。$ F $为对流质量流量(Convective Mass Flux)，$ D $为界面的扩张传导性（Diffusion Conductance）。$ F $、$ D $、$ Pe $计算公式见式\ref{eq:2.20}、\ref{eq:2.21}、\ref{eq：2.22}。$ Pe $表示流动中对流与扩散的大小关系。$ Pe $为0时，流场中没有流动，只有扩散作用；$ Pe $较大时，流动变为纯对流。

\begin{equation}
F=\rho u
\label{eq:2.20}
\end{equation}

\begin{equation}
D=\frac{\varGamma }{\text{ }\!\!\delta\!\!\text{ }x}
\label{eq:2.21}
\end{equation}
\begin{equation}
Pe=\frac{F}{D}=\frac{\rho u\text{ }\!\!\delta\!\!\text{ }x}{\varGamma }
\label{eq：2.22}
\end{equation}

$ S_c $和$ S_P $为源项$ S_\varphi $的线性系数，满足$ S_\varphi=S_C+S_P \varphi_P $。
构建完成共轭传热问题的离散方程组后，使用SIMPLE算法求解压力与速度耦合问题。基本求解过程如下：

\begin{enumerate}
	\item 规定压力、速度等参数初始值，确定离散方程中的系数及常数项；
	\item 联立求解基本状态方程；
	\item 求解湍流方程和其他方程；
	\item 判断是否收敛。若不收敛，返回第二步继续迭代；若收敛，进行下一时间步的计算。
\end{enumerate}

\section{本章小结}

本章介绍了共轭传热问题的求解流程，对低压缸整机共轭传热计算的原理进行了阐述。根据整场求解的思路，首先建立了流体域的控制方程，包括流体的基本状态方程和湍流模型方程，使用通用方程进行统一描述。而后通过定义不同的源项和扩散项，将固体域包括进来。然后使用有限体积法对控制方程进行离散，最后使用SIMPLE算法求解压力和速度的耦合问题。


